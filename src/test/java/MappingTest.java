import eu.tyrano.cyanogen.mappings.*;

class MappingTest {
    private static void printResult(Mapping mapping, int indentation) {
        if (mapping instanceof MappingRoot root) {
            for (Mapping child : root.getChildren()) {
                printResult(child, indentation);
            }
        } else if (mapping instanceof MappingIndex index) {
            System.out.println("    ".repeat(indentation) + index.getIndex());
            if (mapping instanceof MappingRecursiveIndex recursiveIndex) {
                printResult(recursiveIndex.getNext(), indentation + 1);
            }
        } else if (mapping instanceof MappingList list) {
            System.out.println("    ".repeat(indentation) + list.getFrom() + " -> (" + list.getDelimiter() + ") -> " + list.getTo());
            if (mapping instanceof MappingRecursiveList recursiveList) {
                printResult(recursiveList.getNext(), indentation + 1);
            }
        } else if (mapping instanceof MappingText test) {
            System.out.println("    ".repeat(indentation) + test.getText());
        } else if (mapping instanceof MappingCaller) {
            System.out.println("    ".repeat(indentation) + "caller");
        }
    }

    public static void main(String[] args) {
        Mapping result = MappingParser.parse("$caller$_{$0:-1[ , ]$}");
        printResult(result, 0);
    }
}