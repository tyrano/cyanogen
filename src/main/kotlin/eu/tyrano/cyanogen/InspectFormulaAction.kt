package eu.tyrano.cyanogen

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.psi.PsiDocumentManager
import com.intellij.ui.WindowMoveListener
import com.intellij.ui.components.panels.VerticalBox
import com.intellij.ui.popup.AbstractPopup
import org.scilab.forge.jlatexmath.TeXFormula
import java.awt.Image
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.MouseEvent
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import javax.imageio.ImageIO
import javax.swing.ImageIcon
import javax.swing.JFileChooser
import javax.swing.JLabel


class InspectFormulaAction : AnAction() {
    override fun actionPerformed(e: AnActionEvent) {
        val editor: Editor = e.getData(CommonDataKeys.EDITOR) ?: return
        val project = e.project ?: return

        val psiFile = PsiDocumentManager.getInstance(project).getPsiFile(editor.document) ?: return
        val selectionModel = editor.selectionModel

        val start = selectionModel.selectionStart
        val end = selectionModel.selectionEnd

        var latex: String
        try {
            val rootElements = StatementParser.getSelectedElements(psiFile, start - 1, end)
            latex = StatementParser.statementToLaTeX(rootElements)
        } catch (_: Exception) {
            latex = "Unable\\:to\\:display\\:LaTeX!"
        }

        val fgColor = EditorColorsManager.getInstance().globalScheme.defaultForeground
        val bgColor = EditorColorsManager.getInstance().globalScheme.defaultBackground
        val image: BufferedImage = TeXFormula.createBufferedImage(latex, 0, 40f, fgColor, bgColor) as BufferedImage
        val ratio: Double = image.width / image.height.toDouble()

        val imageLabel = JLabel()
        imageLabel.icon = ImageIcon(image)

        val listener: WindowMoveListener = object : WindowMoveListener() {
            override fun mouseClicked(event: MouseEvent?) {
                if (event != null) {
                    if (!event.isConsumed && event.button == 3) {
                        val options = arrayOf("Copy", "Save")
                        val popup = JBPopupFactory.getInstance()
                            .createPopupChooserBuilder(options.toList())
                            .setTitle("Select an Option")
                            .setItemChosenCallback {
                                if (it == "Copy") {
                                    val toCopy = StringSelection(latex)
                                    Toolkit.getDefaultToolkit().systemClipboard.setContents(toCopy, toCopy)
                                    val copied = if (latex.length > 128) "\"${latex.substring(0..<128)}...\"" else latex
                                    Messages.showInfoMessage(project, "Copied $copied to clipboard", "Info")
                                } else {
                                    val fileChooser = JFileChooser()
                                    fileChooser.dialogTitle = "Select Destination to Save Image"
                                    fileChooser.fileSelectionMode = JFileChooser.FILES_ONLY

                                    val userSelection = fileChooser.showSaveDialog(null)

                                    if (userSelection == JFileChooser.APPROVE_OPTION) {
                                        val destination: File = fileChooser.selectedFile
                                        try {
                                            val outputStream = ByteArrayOutputStream()
                                            ImageIO.write(image, "png", outputStream)
                                            destination.writeBytes(outputStream.toByteArray())
                                            Messages.showInfoMessage(
                                                project,
                                                "File saved to ${destination.absolutePath}",
                                                "Success"
                                            )
                                        } catch (e: IOException) {
                                            Messages.showErrorDialog(
                                                project,
                                                "Failed to save file: ${e.message}",
                                                "Error"
                                            )
                                        }
                                    }
                                }
                            }.createPopup()
                        popup.showInScreenCoordinates(imageLabel, event.locationOnScreen)
                        popup.setRequestFocus(true)
                        event.consume()
                    }
                }
            }
        }

        val mainPanel = VerticalBox()

        val popup: AbstractPopup = JBPopupFactory.getInstance().createComponentPopupBuilder(mainPanel, null)
            .setMovable(true)
            .setFocusable(true)
            .setRequestFocus(true)
            .setResizable(true)
            .createPopup() as AbstractPopup

        listener.installTo(popup.component)

        popup.addResizeListener({
            val width = imageLabel.width
            val height = imageLabel.height

            if (width > 0 && height > 0) {
                if (width / height.toDouble() > ratio) {
                    imageLabel.icon = ImageIcon(
                        image.getScaledInstance(
                            width,
                            (width / ratio).toInt(),
                            Image.SCALE_SMOOTH
                        )
                    )
                } else {
                    imageLabel.icon = ImageIcon(
                        image.getScaledInstance(
                            (height * ratio).toInt(),
                            height,
                            Image.SCALE_SMOOTH
                        )
                    )
                }
            }
        }, popup)

        mainPanel.add(imageLabel)

        popup.show(JBPopupFactory.getInstance().guessBestPopupLocation(e.dataContext))
    }
}
