package eu.tyrano.cyanogen.settings

import com.intellij.openapi.fileChooser.FileChooser
import com.intellij.openapi.fileChooser.FileChooserDescriptor
import com.intellij.openapi.fileEditor.impl.LoadTextUtil
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.vfs.VirtualFile
import org.jetbrains.annotations.Nls
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JPanel


class CyanogenConfigurable : Configurable {
    private var panel: JPanel? = null

    override fun getDisplayName(): @Nls(capitalization = Nls.Capitalization.Title) String {
        return "Cyanogen"
    }

    override fun getPreferredFocusedComponent(): JComponent? {
        return null
    }

    override fun createComponent(): JComponent? {
        panel = JPanel()
        val buttonImport = JButton("Import configurations")
        panel!!.add(buttonImport)

        buttonImport.addActionListener {
            FileChooser.chooseFile(
                FileChooserDescriptor(true, false, false, false, false, false),
                null,
                null
            ) {
                import(it)
            }
        }

        val resetButton = JButton("Reset to default")
        panel!!.add(resetButton)

        resetButton.addActionListener {
            JBPopupFactory.getInstance().createConfirmation("Reset Every Cyanogen Configurations To Default", {
                resetConfigurations()
            }, 1).showInCenterOf(resetButton)
        }

        return panel
    }

    private fun resetConfigurations() {
        val settings = SettingsState.instance
        settings.constants = DefaultSettings.constants
        settings.functions = DefaultSettings.functions
        settings.objects = DefaultSettings.objects
    }

    private fun import(file: VirtualFile) {
        try {
            val jsonString = LoadTextUtil.loadText(file).toString()
            val settings = SettingsState.instance

            val jsonObject = JSONObject(jsonString)

            settings.constants += getArrayContent(jsonObject.getJSONArray("constants"))
            settings.functions += getArrayContent(jsonObject.getJSONArray("functions"))
            settings.objects += getArrayContent(jsonObject.getJSONArray("objects"))
        } catch (_: JSONException) {
        }
    }

    private fun getArrayContent(jsonArray: JSONArray): MutableList<SettingEntry> {
        val list = mutableListOf<SettingEntry>()
        for (i in 0 until jsonArray.length()) {
            val obj = jsonArray.getJSONObject(i)
            val group = obj.getString("group")
            val key = obj.getString("key")
            val value = obj.getString("value")
            list.add(SettingEntry(group, key, value))
        }
        return list
    }

    override fun isModified(): Boolean {
        return false
    }

    override fun apply() {
    }

    override fun reset() {
    }
}