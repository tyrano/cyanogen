package eu.tyrano.cyanogen.settings

import java.io.Serializable


data class SettingEntry(var group: String, var key: String, var value: String) : Serializable {
    @Suppress("unused")
    constructor() : this("", "", "")
}

