package eu.tyrano.cyanogen.settings

class DefaultSettings {
    companion object {
        var constants = listOf(
            SettingEntry("java_math", "java.lang.Math.PI", "\\pi"),
            SettingEntry("java_math", "java.lang.Math.E", "e"),
            SettingEntry("java_math", "java.lang.Math.TAU", "\\tau"),
        )

        var functions = listOf(
            SettingEntry("internal", "array()", "\$caller\$_{\$0:-1[ , ]\$}"),
            SettingEntry(
                "internal",
                "ternary()",
                "\\begin{cases}\$1\$ & & \\text{if } \$0\$ \\\\ \$2\$ & & \\text{otherwise} \\end{cases}"
            ),
            SettingEntry("internal", "()", "\\left(\$0\$\\right)"),
            SettingEntry("internal", "+()", "\\left(+\$0\$\\right)"),
            SettingEntry("internal", "-()", "\\left(-\$0\$\\right)"),
            SettingEntry("internal", "~()", "\\left(\\sim \$0\$\\right)"),
            SettingEntry("internal", "!()", "\\left(\\neg \$0\$\\right)"),
            SettingEntry("internal", "++()", "\\left(+\\!+\\!\$0\$\\right)"),
            SettingEntry("internal", "--()", "\\left(-\\!-\\!\$0\$\\right)"),
            SettingEntry("internal", "()++", "\\left(\$0\$+\\!+\\!\\right)"),
            SettingEntry("internal", "()--", "\\left(\$0\$-\\!-\\!\\right)"),
            SettingEntry("internal", "double()", "double\\left(\$0\$\\right)"),
            SettingEntry("internal", "float()", "float\\left(\$0\$\\right)"),
            SettingEntry("internal", "long()", "long\\left(\$0\$\\right)"),
            SettingEntry("internal", "int()", "int\\left(\$0\$\\right)"),
            SettingEntry("internal", "char()", "char\\left(\$0\$\\right)"),
            SettingEntry("internal", "short()", "short\\left(\$0\$\\right)"),
            SettingEntry("internal", "byte()", "byte\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.sqrt double", "\\sqrt{\$0\$}"),
            SettingEntry("java_math", "java.lang.Math.cbrt double", "\\sqrt[3]{\$0\$}"),
            SettingEntry("java_math", "java.lang.Math.abs int", "\\left|\$0\$\\right|"),
            SettingEntry("java_math", "java.lang.Math.abs long", "\\left|\$0\$\\right|"),
            SettingEntry("java_math", "java.lang.Math.abs float", "\\left|\$0\$\\right|"),
            SettingEntry("java_math", "java.lang.Math.abs double", "\\left|\$0\$\\right|"),
            SettingEntry("java_math", "java.lang.Math.asin double", "\\sin^{-1}\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.acos double", "\\cos^{-1}\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.atan double", "\\tan^{-1}\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.log double", "\\ln\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.log10 double", "\\log_{10}\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.ceil double", "\\left\\lceil{\$0\$}\\right\\rceil"),
            SettingEntry("java_math", "java.lang.Math.floor double", "\\left\\lceil{\$0\$}\\right\\rceil"),
            SettingEntry("java_math", "java.lang.Math.signum double", "sgn\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.signum float", "sgn\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.pow double double", "\$0\$^{\$1\$}"),
            SettingEntry("java_math", "java.lang.Math.sin double", "sin\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.cos double", "cos\\left(\$0\$\\right)"),
            SettingEntry("java_math", "java.lang.Math.tan double", "tan\\left(\$0\$\\right)"),
        )

        var objects = listOf<SettingEntry>(
        )
    }
}