package eu.tyrano.cyanogen.settings

import com.intellij.openapi.options.Configurable
import com.intellij.ui.ToolbarDecorator
import com.intellij.ui.table.JBTable
import org.jetbrains.annotations.Nls
import javax.swing.JComponent
import javax.swing.ListSelectionModel
import javax.swing.table.DefaultTableModel


abstract class TableConfigurable : Configurable {
    private var mySettingsComponent: JBTable? = null

    abstract override fun getDisplayName(): @Nls(capitalization = Nls.Capitalization.Title) String

    override fun getPreferredFocusedComponent(): JComponent? {
        return null
    }

    abstract fun getContent(): List<SettingEntry>

    abstract fun setContent(content: List<SettingEntry>)

    abstract fun getColumnNames(): Array<String>

    override fun createComponent(): JComponent {
        val functionArray = getContent()

        var rowIndex = 0
        val biArray = Array(functionArray.size) { Array<String?>(3) { null } }
        for ((group, key, value) in functionArray) {
            biArray[rowIndex][0] = group
            biArray[rowIndex][1] = key
            biArray[rowIndex][2] = value
            rowIndex++
        }

        val model = DefaultTableModel()
        model.setDataVector(biArray, getColumnNames())

        mySettingsComponent = JBTable(model)
        mySettingsComponent!!.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)

        return ToolbarDecorator.createDecorator(mySettingsComponent!!)
            .disableUpAction()
            .disableDownAction()
            .setAddAction { addBasePackageModules() }
            .setRemoveAction { removeSelectedBasePackageModules() }
            .createPanel()
    }

    private fun addBasePackageModules() {
        (mySettingsComponent?.model as DefaultTableModel).addRow(arrayOf("custom", "", ""))
    }

    private fun removeSelectedBasePackageModules() {
        val selectedRows = mySettingsComponent?.selectedRows!!

        if (selectedRows.isNotEmpty()) {
            for (row in selectedRows) {
                (mySettingsComponent?.model as DefaultTableModel).removeRow(row)
            }
        }
    }

    override fun isModified(): Boolean {
        val model: DefaultTableModel = mySettingsComponent?.model as DefaultTableModel
        val dataClassArray = model.dataVector.map { strings ->
            SettingEntry(strings[0] as String, strings[1] as String, strings[2] as String)
        }
        return dataClassArray != getContent()
    }

    override fun apply() {
        val model: DefaultTableModel = mySettingsComponent?.model as DefaultTableModel
        val dataClassArray = model.dataVector.map { strings ->
            SettingEntry(strings[0] as String, strings[1] as String, strings[2] as String)
        }
        setContent(dataClassArray)
    }

    override fun reset() {
        val functionArray = getContent()
        var rowIndex = 0
        val biArray = Array(functionArray.size) { Array<String?>(3) { null } }
        for ((group, key, value) in functionArray) {
            biArray[rowIndex][0] = group
            biArray[rowIndex][1] = key
            biArray[rowIndex][2] = value
            rowIndex++
        }

        val model: DefaultTableModel = mySettingsComponent?.model as DefaultTableModel
        model.setDataVector(biArray, getColumnNames())
    }

    override fun disposeUIResources() {
        mySettingsComponent = null
    }
}