package eu.tyrano.cyanogen.settings

import org.jetbrains.annotations.Nls


class FunctionConfigurable : TableConfigurable() {
    override fun getDisplayName(): @Nls(capitalization = Nls.Capitalization.Title) String {
        return "Cyanogen Functions"
    }

    override fun getContent(): List<SettingEntry> {
        return SettingsState.instance.functions
    }

    override fun setContent(content: List<SettingEntry>) {
        val state = SettingsState.instance
        state.functions = content
        state.cachedMappings.clear()
    }

    override fun getColumnNames(): Array<String> {
        return arrayOf("Group", "Function", "LaTeX")
    }
}