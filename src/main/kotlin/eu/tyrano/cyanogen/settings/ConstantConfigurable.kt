package eu.tyrano.cyanogen.settings

import org.jetbrains.annotations.Nls


class ConstantConfigurable : TableConfigurable() {
    override fun getDisplayName(): @Nls(capitalization = Nls.Capitalization.Title) String {
        return "Cyanogen Constants"
    }

    override fun getContent(): List<SettingEntry> {
        return SettingsState.instance.constants
    }

    override fun setContent(content: List<SettingEntry>) {
        val state = SettingsState.instance
        state.constants = content
        state.cachedMappings.clear()
    }

    override fun getColumnNames(): Array<String> {
        return arrayOf("Group", "Constant", "LaTeX")
    }
}