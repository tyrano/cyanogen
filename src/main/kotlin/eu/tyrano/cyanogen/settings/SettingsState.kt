package eu.tyrano.cyanogen.settings

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.util.xmlb.XmlSerializerUtil
import com.intellij.util.xmlb.annotations.Transient
import com.intellij.util.xmlb.annotations.XCollection
import eu.tyrano.cyanogen.mappings.Mapping

@State(name = "eu.tyrano.cyanogen.settings.SettingsState", storages = [Storage("CyanogenPlugin.xml")])
class SettingsState : PersistentStateComponent<SettingsState?> {

    @XCollection
    var constants = DefaultSettings.constants

    @XCollection
    var functions = DefaultSettings.functions

    @XCollection
    var objects: List<SettingEntry> = DefaultSettings.objects

    @Transient
    val cachedMappings: MutableMap<SettingEntry, Mapping> = mutableMapOf()

    override fun getState(): SettingsState {
        return this
    }

    override fun loadState(state: SettingsState) {
        XmlSerializerUtil.copyBean(state, this)
    }

    companion object {
        val instance: SettingsState
            get() = ApplicationManager.getApplication().getService(SettingsState::class.java)
    }
}