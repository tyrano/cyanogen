package eu.tyrano.cyanogen.settings

import org.jetbrains.annotations.Nls


class ObjectsConfigurable : TableConfigurable() {
    override fun getDisplayName(): @Nls(capitalization = Nls.Capitalization.Title) String {
        return "Cyanogen Objects"
    }

    override fun getContent(): List<SettingEntry> {
        return SettingsState.instance.objects
    }

    override fun setContent(content: List<SettingEntry>) {
        val state = SettingsState.instance
        state.objects = content
        state.cachedMappings.clear()
    }

    override fun getColumnNames(): Array<String> {
        return arrayOf("Group", "Object", "LaTeX")
    }
}