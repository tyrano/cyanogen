package eu.tyrano.cyanogen.nodes

import eu.tyrano.cyanogen.nodes.accessors.InternalAccessor

class ArrayAccessNode(objectName: Node) : DynamicFunctionNode(InternalAccessor("array()"), "array()", objectName)