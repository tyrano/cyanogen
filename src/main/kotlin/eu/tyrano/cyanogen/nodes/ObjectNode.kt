package eu.tyrano.cyanogen.nodes

import eu.tyrano.cyanogen.nodes.accessors.Accessor

open class ObjectNode(val fqn: Accessor, val codeObject: String) : Node()