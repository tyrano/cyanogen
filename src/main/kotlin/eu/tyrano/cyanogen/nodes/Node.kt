package eu.tyrano.cyanogen.nodes

abstract class Node {
    private val children: MutableList<Node> = ArrayList()

    open fun addChild(child: Node) {
        children.add(child)
    }

    open fun getChildren(): List<Node> {
        return children
    }
}