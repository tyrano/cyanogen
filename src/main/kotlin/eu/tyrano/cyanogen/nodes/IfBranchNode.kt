package eu.tyrano.cyanogen.nodes

import eu.tyrano.cyanogen.Statement

class IfBranchNode(val condition: Node?, val statements: Array<Statement>): Node()