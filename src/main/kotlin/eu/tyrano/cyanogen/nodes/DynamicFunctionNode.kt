package eu.tyrano.cyanogen.nodes

import eu.tyrano.cyanogen.nodes.accessors.Accessor

open class DynamicFunctionNode(accessor: Accessor, codeFunction: String, val objectName: Node) : FunctionNode(accessor, codeFunction)