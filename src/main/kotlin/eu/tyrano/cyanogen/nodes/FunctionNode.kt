package eu.tyrano.cyanogen.nodes

import eu.tyrano.cyanogen.nodes.accessors.Accessor

open class FunctionNode(val fqn: Accessor, val codeFunction: String) : Node()