package eu.tyrano.cyanogen.nodes.accessors

class InternalAccessor(private val internalFunction: String) : Accessor() {
    override fun matches(key: String): Boolean {
        return internalFunction == key
    }
}