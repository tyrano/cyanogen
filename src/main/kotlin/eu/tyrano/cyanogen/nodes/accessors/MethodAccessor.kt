package eu.tyrano.cyanogen.nodes.accessors

class MethodAccessor(private val matchingFQNs: List<String>, private val paramTypes: List<String>): Accessor() {
    override fun matches(key: String): Boolean {
        val fqnAndParams = key.split(" ", limit = 2)
        if (matchingFQNs.any { it == fqnAndParams[0] }) {
            return if (fqnAndParams.size == 1) {
                paramTypes.isEmpty()
            } else {
                val params = fqnAndParams[1].split(" ")
                paramTypes == params
            }
        }
        return false
    }
}