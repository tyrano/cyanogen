package eu.tyrano.cyanogen.nodes.accessors

class ConstructorAccessor(private val fqn: String, private val paramTypes: List<String>) : Accessor() {
    override fun matches(key: String): Boolean {
        val fqnAndParams = key.split(" ", limit = 2)
        if (fqn == fqnAndParams[0]) {
            return if (fqnAndParams.size == 1) {
                paramTypes.isEmpty()
            } else {
                val params = fqnAndParams[1].split(" ")
                paramTypes == params
            }
        }
        return false
    }
}