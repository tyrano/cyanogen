package eu.tyrano.cyanogen.nodes.accessors

abstract class Accessor {
    abstract fun matches(key: String): Boolean
}