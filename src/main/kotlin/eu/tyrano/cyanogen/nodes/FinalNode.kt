package eu.tyrano.cyanogen.nodes

class FinalNode(val name: String, val fqn: String) : Node() {
    override fun addChild(child: Node) {
        throw UnsupportedOperationException()
    }

    override fun getChildren(): List<Node> {
        throw UnsupportedOperationException()
    }
}