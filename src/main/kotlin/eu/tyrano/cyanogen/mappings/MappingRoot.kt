package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.nodes.Node

class MappingRoot(val children: List<Mapping>) : Mapping() {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        var result = ""
        for (child in children) {
            result += child.apply(nodes, caller)
        }
        return result
    }
}