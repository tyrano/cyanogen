package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.LatexConverter
import eu.tyrano.cyanogen.nodes.Node

open class MappingIndex(val index: Int) : Mapping() {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        if (index < 0) {
            return LatexConverter.latex(nodes[nodes.size + index])
        }
        return LatexConverter.latex(nodes[index])
    }
}