package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.nodes.Node

class MappingRecursiveList(delimiter: String, from: Int, to: Int, val next: Mapping) : MappingList(delimiter, from, to) {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        val to =  if (to < 0) {
            nodes.size + to
        } else {
            to
        }

        val from =  if (from < 0) {
            nodes.size + from
        } else {
            from
        }

        var builder = ""
        for (i in from..to) {
            builder += next.apply(nodes[i].getChildren(), caller) + delimiter
        }
        return builder.removeSuffix(delimiter)
    }
}