package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.nodes.Node

class MappingText(val text: String): Mapping() {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        return text
    }
}