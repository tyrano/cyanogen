package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.nodes.Node

class MappingRecursiveIndex(index: Int, val next: Mapping) : MappingIndex(index) {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        if (index < 0) {
            return next.apply(nodes[nodes.size + index].getChildren(), caller)
        }
        return next.apply(nodes[index].getChildren(), caller)
    }
}