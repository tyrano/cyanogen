package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.nodes.Node

abstract class Mapping {
    abstract fun apply(nodes: List<Node>, caller: Node?): String
}