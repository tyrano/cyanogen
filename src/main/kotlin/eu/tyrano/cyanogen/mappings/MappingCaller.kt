package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.LatexConverter
import eu.tyrano.cyanogen.nodes.Node

class MappingCaller : Mapping() {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        return LatexConverter.latex(caller ?: return "")
    }
}