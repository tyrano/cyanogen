package eu.tyrano.cyanogen.mappings

import eu.tyrano.cyanogen.LatexConverter
import eu.tyrano.cyanogen.nodes.Node

open class MappingList(val delimiter: String, val from: Int, val to: Int): Mapping() {
    override fun apply(nodes: List<Node>, caller: Node?): String {
        val to =  if (to < 0) {
            nodes.size + to
        } else {
            to
        }

        val from =  if (from < 0) {
            nodes.size + from
        } else {
            from
        }

        var builder = ""
        for (i in from..to) {
            builder += LatexConverter.latex(nodes[i]) + delimiter
        }
        return builder.removeSuffix(delimiter)
    }
}