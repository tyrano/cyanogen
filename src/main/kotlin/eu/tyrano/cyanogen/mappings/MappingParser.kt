package eu.tyrano.cyanogen.mappings

import java.util.*

object MappingParser {
    private fun findIndex(content: String): Int {
        val stack = Stack<String>()
        for (i in 0..content.length - 2) {
            val at = content.substring(i, i + 2)
            if (at == "\${" || at == "#{") {
                stack.push(at)
            } else if (at == "}\$") {
                if (stack.peek() == "\${" || stack.peek() == "#{") {
                    stack.pop()
                    if (stack.isEmpty()) {
                        return i
                    }
                }
            }
        }
        return -1
    }

    private fun parseGroup(content: String): MappingResult? {
        val closingIndex = findIndex(content)
        return if (closingIndex >= 0) {
            val groupContent = content.substring(2, closingIndex)
            MappingResult(parse(groupContent), closingIndex + 2)
        } else {
            null
        }
    }

    private fun parseCaller(): MappingResult {
        return MappingResult(MappingCaller(), "\$caller\$".length)
    }

    private fun parseIndex(content: String): MappingResult {
        val index = content.removePrefix("\$").removeSuffix("\$").toInt()
        return MappingResult(MappingIndex(index), content.length)
    }

    private fun parseList(content: String): MappingResult {
        val indices = content.split("[")[0].removePrefix("\$").split(":")
        val startIndex = indices[0].toInt()
        val endIndex = indices[1].toInt()
        val delimiter = content.split("[")[1].removeSuffix("]\$")
        return MappingResult(MappingList(delimiter, startIndex, endIndex), content.length)
    }

    private fun parseIndexArray(content: String): MappingResult? {
        val matcher = Regex("\\$-?[0-9]+#").find(content)
        if (matcher != null) {
            val matched = matcher.value
            val index = matched.removePrefix("\$").removeSuffix("#").toInt()

            val nextResult = parseExpression("\$" + content.replaceFirst(matched, "")) ?: return null

            return MappingResult(MappingRecursiveIndex(index, nextResult.mapping), matched.length + nextResult.size - 1)
        } else {
            return null
        }
    }

    private fun parseListArray(content: String): MappingResult? {
        val matcher = Regex("\\\$-?[0-9]+:-?[0-9]+\\[.*?]#").find(content)
        if (matcher != null) {
            val matched = matcher.value
            val indices = matched.split("[")[0].removePrefix("\$")
            val startIndex = indices.split(":")[0].toInt()
            val endIndex = indices.split(":")[1].toInt()
            val delimiter = matched.split("[")[1].removeSuffix("]#")

            val nextResult = parseExpression("\$" + content.replaceFirst(matched, "")) ?: return null

            return MappingResult(
                MappingRecursiveList(delimiter, startIndex, endIndex, nextResult.mapping),
                matched.length + nextResult.size - 1
            )
        } else {
            return null
        }
    }

    private fun parseExpression(content: String): MappingResult? {
        return if (content.startsWith("\${")) {
            parseGroup(content)
        } else if (content.startsWith("\$caller\$")) {
            parseCaller()
        } else {
            val indexArrayMatcher = Regex("^\\$-?[0-9]+#.*").find(content)
            if (indexArrayMatcher != null) {
                parseIndexArray(indexArrayMatcher.value)
            } else {
                val listArrayMatcher = Regex("^\\$-?[0-9]+:-?[0-9]+\\[.*?]#.*").find(content)
                if (listArrayMatcher != null) {
                    parseListArray(listArrayMatcher.value)
                } else {
                    val indexMatcher = Regex("^\\$-?[0-9]+\\$").find(content)
                    if (indexMatcher != null) {
                        parseIndex(indexMatcher.value)
                    } else {
                        val listMatcher = Regex("^\\$-?[0-9]+:-?[0-9]+\\[.*?]\\$").find(content)
                        if (listMatcher != null) {
                            parseList(listMatcher.value)
                        } else {
                            null
                        }
                    }
                }
            }
        }
    }

    @JvmStatic
    fun parse(content: String): Mapping {
        if (content.contains("$")) {
            val mappings: MutableList<Mapping> = mutableListOf()

            var indexOfDollar = content.indexOf("$")
            if (indexOfDollar > 0) {
                mappings.add(MappingText(content.substring(0, indexOfDollar)))
            }
            while (indexOfDollar >= 0 && indexOfDollar < content.length) {
                val parsed: MappingResult? = parseExpression(content.substring(indexOfDollar))

                if (parsed == null) {
                    indexOfDollar = content.indexOf("$", indexOfDollar + 1)
                    mappings.add(MappingText("$"))
                } else {
                    val previousExpression = indexOfDollar + parsed.size
                    mappings.add(parsed.mapping)
                    indexOfDollar = content.indexOf("$", previousExpression)
                    if (indexOfDollar < 0) {
                        mappings.add(MappingText(content.substring(previousExpression, content.length)))
                    } else {
                        mappings.add(MappingText(content.substring(previousExpression, indexOfDollar)))
                    }
                }
            }


            return MappingRoot(mappings)
        } else {
            return MappingText(content)
        }
    }

    private class MappingResult(val mapping: Mapping, val size: Int)
}