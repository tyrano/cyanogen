package eu.tyrano.cyanogen

import eu.tyrano.cyanogen.mappings.MappingParser
import eu.tyrano.cyanogen.nodes.*
import eu.tyrano.cyanogen.settings.SettingEntry
import eu.tyrano.cyanogen.settings.SettingsState

object LatexConverter {
    fun latex(node: Node): String {
        val state = SettingsState.instance
        when (node) {
            is FinalNode -> {
                for (mapping in state.constants) {
                    if (mapping.key == node.fqn) {
                        return mapping.value
                    }
                }

                return node.name
            }

            is OperationNode -> {
                return when (node.operation) {
                    "+" -> {
                        formatOperation(node, "+")
                    }

                    "-" -> {
                        formatOperation(node, "-")
                    }

                    "*" -> {
                        formatOperation(node, "\\cdot")
                    }

                    "/" -> {
                        formatFraction(node.getChildren())
                    }

                    "%" -> {
                        formatOperation(node, "\\mod")
                    }

                    "&" -> {
                        formatOperation(node, "\\land")
                    }

                    "|" -> {
                        formatOperation(node, "\\lor")
                    }

                    "^" -> {
                        formatOperation(node, "\\oplus")
                    }

                    "<<" -> {
                        formatOperation(node, "\\ll")
                    }

                    ">>" -> {
                        formatOperation(node, "\\gg")
                    }

                    ">>>" -> {
                        formatOperation(node, ">\\!\\!\\!>\\!\\!\\!>")
                    }

                    "||" -> {
                        formatOperation(node, "\\lor")
                    }

                    "&&" -> {
                        formatOperation(node, "\\land")
                    }

                    "==" -> {
                        formatOperation(node, "=")
                    }

                    "!=" -> {
                        formatOperation(node, "\\neq")
                    }

                    "<=" -> {
                        formatOperation(node, "\\le")
                    }

                    ">=" -> {
                        formatOperation(node, "\\ge")
                    }

                    "<" -> {
                        formatOperation(node, "<")
                    }

                    ">" -> {
                        formatOperation(node, ">")
                    }

                    else -> {
                        throw ParsingException("Unknown operation")
                    }
                }
            }

            is FunctionNode -> {
                for (mapping in state.functions) {
                    if (node.fqn.matches(mapping.key.trim())) {
                        return parse(mapping, node, state)
                    }
                }

                var builder = if (node is DynamicFunctionNode) {
                    "${latex(node.objectName)}.${node.codeFunction}\\left("
                } else {
                    "${node.codeFunction}\\left("
                }

                for (child in node.getChildren()) {
                    builder += latex(child) + ","
                }
                return builder.removeSuffix(",") + "\\right)"
            }

            is ObjectNode -> {
                for (mapping in state.objects) {
                    if (node.fqn.matches(mapping.key.trim())) {
                        return parse(mapping, node, state)
                    }
                }

                var builder = "${node.codeObject}\\left("
                for (child in node.getChildren()) {
                    builder += latex(child) + ","
                }
                return builder.removeSuffix(",") + "\\right)"
            }

            is IfStatementNode -> {
                // "\begin{cases}$1$ & & \text{if } $0$ \\ $2$ & & \text{otherwise} \end{cases}"

                var builder = "\\begin{cases}"
                for (child in node.getChildren()) {
                    if (child is IfBranchNode) {
                        val inner = StatementParser.statementToLaTeX(child.statements)

                        builder += "$inner & & \\text"
                        builder += if (child.condition != null) {
                            "{if } " + latex(child.condition) + "\\\\"
                        } else {
                            "{otherwise}"
                        }
                        builder += "\\\\"
                    } else {
                        throw ParsingException("Illegal node in if statement node: $child")
                    }
                }
                return builder.removeSuffix("\\\\") + "\\end{cases}"
            }

            is ArrayCreationNode -> {
                throw ParsingException("Array creation nodes cannot be converted to LaTeX")
            }

            else -> {
                throw ParsingException("Unknown node type")
            }
        }
    }

    private fun parse(mapping: SettingEntry, node: Node, state: SettingsState): String {
        var rootMapping = state.cachedMappings[mapping]

        rootMapping = if (rootMapping != null) {
            rootMapping
        } else {
            val newMapping = MappingParser.parse(mapping.value)
            state.cachedMappings[mapping] = newMapping
            newMapping
        }

        return if (node is DynamicFunctionNode) {
            rootMapping.apply(node.getChildren(), node.objectName)
        } else {
            rootMapping.apply(node.getChildren(), null)
        }
    }

    private fun formatFraction(node: List<Node>): String {
        return if (node.size == 2) {
            "\\frac{" + latex(node[0]) + "}{" + latex(node[1]) + "}"
        } else {
            "\\frac{" + formatFraction(node.take(node.size - 1)) + "}{" + latex(node[node.size - 1]) + "}"
        }
    }

    private fun formatOperation(node: Node, operation: String): String {
        var builder = ""
        for (child in node.getChildren()) {
            builder += latex(child) + " " + operation + " "
        }
        return builder.dropLast(2 + operation.length)
    }
}