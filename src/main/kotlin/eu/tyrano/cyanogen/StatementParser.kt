package eu.tyrano.cyanogen

import com.intellij.psi.*
import com.intellij.psi.impl.source.PsiJavaCodeReferenceElementImpl
import com.intellij.psi.impl.source.tree.JavaElementType
import eu.tyrano.cyanogen.nodes.*
import eu.tyrano.cyanogen.nodes.accessors.ConstructorAccessor
import eu.tyrano.cyanogen.nodes.accessors.InternalAccessor
import eu.tyrano.cyanogen.nodes.accessors.MethodAccessor

object StatementParser {
    fun statementToLaTeX(rootElements: Array<Statement>): String {
        var latexBuffer = ""
        for (root in rootElements) {
            try {
                latexBuffer += if (root.name != null) {
                    when (root.operator) {

                        "=" -> root.name + " = " + LatexConverter.latex(parse(root.expression)) + "\\\\ "
                        "+=" -> root.name + " = " + root.name + " + " + LatexConverter.latex(parse(root.expression)) + "\\\\ "
                        "-=" -> root.name + " = " + root.name + " - " + LatexConverter.latex(parse(root.expression)) + "\\\\ "
                        "*=" -> root.name + " = " + root.name + " \\cdot \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        "/=" -> root.name + " = \\frac{" + root.name + "}{" + LatexConverter.latex(parse(root.expression)) + "}\\\\ "
                        "%=" -> root.name + " = " + root.name + " \\mod \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        "&=" -> root.name + " = " + root.name + " \\land \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        "|=" -> root.name + " = " + root.name + " \\lor \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        "^=" -> root.name + " = " + root.name + " \\oplus \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        "<<=" -> root.name + " = " + root.name + " \\ll \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        ">>=" -> root.name + " = " + root.name + " \\gg \\left(" + LatexConverter.latex(parse(root.expression)) + "\\right)\\\\ "
                        ">>>=" -> root.name + " = " + root.name + " >\\\\!\\\\!\\\\!>\\\\!\\\\!\\\\!> \\left(" + LatexConverter.latex(
                            parse(root.expression)
                        ) + "\\right)\\\\ "

                        else -> throw ParsingException("Illegal assignment operator")
                    }
                } else {
                    LatexConverter.latex(parse(root.expression)) + "\\\\ "
                }
            } catch (_: ParsingException) {
            }
        }
        return latexBuffer.dropLast(3)
    }


    fun getSelectedElements(psiFile: PsiFile, startOffset: Int, endOffset: Int): Array<Statement> {
        val selectedElements: MutableList<Statement> = mutableListOf()
        var element = psiFile.findElementAt(startOffset)
        while (element != null && element.textRange.endOffset <= endOffset) {
            try {
                if (isMathExpression(element)) {
                    return arrayOf(Statement(null, null, element))
                } else if (isMathStatement(element)) {
                    val statement = statement(element)
                    if (statement != null) {
                        selectedElements.add(statement)
                    }
                } else if (isConditionalStatement(element)) {
                    selectedElements.add(Statement(null, null, element))
                }
                element = element.nextSibling
            } catch (_: ParsingException) {
            }
        }
        return selectedElements.toTypedArray<Statement>()
    }

    private fun statement(element: PsiElement): Statement? {
        var name: String? = null
        var operator: String? = null
        var expression: PsiElement? = null

        for (child in element.children[0].children) {
            val assignOperator = getAssignOperator(child)
            if (assignOperator != null) {
                operator = assignOperator
            } else if (operator == null
                && (child.node.elementType === JavaTokenType.IDENTIFIER
                        || child.node.elementType === JavaElementType.REFERENCE_EXPRESSION)
            ) {
                name = child.text
            } else if (isMathExpression(child)) {
                expression = child
            }
        }

        if (expression == null) {
            return null
        }

        return Statement(name!!, operator!!, expression)
    }

    @Throws(ParsingException::class)
    private fun parse(root: PsiElement): Node {
        val rootType = root.node.elementType
        return when (rootType) {
            JavaElementType.BINARY_EXPRESSION -> operation(root)
            JavaElementType.METHOD_CALL_EXPRESSION -> function(root)
            JavaElementType.PREFIX_EXPRESSION -> prefix(root)
            JavaElementType.POSTFIX_EXPRESSION -> postfix(root)
            JavaElementType.REFERENCE_EXPRESSION -> final(root)
            JavaElementType.LITERAL_EXPRESSION -> final(root)
            JavaElementType.POLYADIC_EXPRESSION -> polyadic(root)
            JavaElementType.TYPE_CAST_EXPRESSION -> cast(root)
            JavaElementType.CONDITIONAL_EXPRESSION -> conditional(root)
            JavaElementType.NEW_EXPRESSION -> new(root)
            JavaElementType.ARRAY_ACCESS_EXPRESSION -> array(root)
            JavaElementType.IF_STATEMENT -> ifStatement(root)
            else -> parenthesis(root)
        }
    }

    private fun array(root: PsiElement): Node {
        return arrayRecursive(root, mutableListOf())
    }

    private fun arrayRecursive(root: PsiElement, accessors: MutableList<Node>): Node {
        var caller: PsiElement? = null
        var index: PsiElement? = null
        for (child in root.children) {
            if (isMathExpression(child)) {
                if (caller == null) {
                    caller = child
                } else {
                    index = child
                }
            }
        }

        if (caller == null) {
            throw ParsingException("Array access on nothing")
        }

        if (index == null) {
            throw ParsingException("No array access index")
        }

        if (caller.node.elementType === JavaElementType.ARRAY_ACCESS_EXPRESSION) {
            accessors.add(parse(index))
            return arrayRecursive(caller, accessors)
        }

        val node = ArrayAccessNode(parse(caller))
        node.addChild(parse(index))
        for (accessor in accessors.reversed()) {
            node.addChild(accessor)
        }
        return node
    }

    private fun new(root: PsiElement): Node {
        var objectName: String? = null
        val parameters: MutableList<Node> = mutableListOf()
        var accessor: ConstructorAccessor? = null

        if (root is PsiNewExpression) {
            val resolved = root.resolveConstructor()
            if (resolved is PsiMethod) {
                accessor = findConstructor(resolved)
            }
        }

        for (child in root.children) {
            if (child.node.elementType === JavaElementType.JAVA_CODE_REFERENCE) {
                objectName = child.text
                if (accessor == null) {
                    val codeReferenceChild = (child as PsiJavaCodeReferenceElementImpl)
                    val resolved = codeReferenceChild.resolve()
                    accessor = ConstructorAccessor(
                        getFullyQualifiedName(
                            codeReferenceChild,
                            resolved,
                            codeReferenceChild.qualifiedName
                        ), emptyList()
                    )
                }
            } else if (child.node.elementType === JavaElementType.EXPRESSION_LIST) {
                for (parameter in child.children) {
                    if (parameter.node.elementType === JavaElementType.NEW_EXPRESSION) {
                        val node = stackArrayCreation(parameter)
                        if (node != null) {
                            parameters.add(node)
                            continue
                        }
                    }
                    if (isMathExpression(parameter)) {
                        parameters.add(parse(parameter))
                    }
                }
            }
        }
        if (objectName == null) {
            throw ParsingException("Constructor has no type")
        }

        if (accessor == null) {
            throw ParsingException("No accessor created")
        }

        val node = ObjectNode(accessor, objectName)

        for (parameter in parameters) {
            node.addChild(parameter)
        }

        return node
    }

    private fun conditional(root: PsiElement): Node {
        var booleanExpression: PsiElement? = null
        var ifExpression: PsiElement? = null
        var elseExpression: PsiElement? = null
        for (child in root.children) {
            if (isMathExpression(child)) {
                if (booleanExpression == null) {
                    booleanExpression = child
                } else if (ifExpression == null) {
                    ifExpression = child
                } else {
                    elseExpression = child
                }
            }
        }

        if (booleanExpression == null || ifExpression == null || elseExpression == null) {
            throw ParsingException("Ternary statement is not complete")
        }

        val node = FunctionNode(InternalAccessor("ternary()"), "ternary()")
        node.addChild(parse(booleanExpression))
        node.addChild(parse(ifExpression))
        node.addChild(parse(elseExpression))
        return node
    }

    private fun cast(root: PsiElement): Node {
        var castType: String? = null
        var expression: PsiElement? = null
        for (child in root.children) {
            if (child.node.elementType === JavaElementType.TYPE) {
                val type = getPrimitiveType(child.children[0])
                if (type != null) {
                    castType = "$type()"
                }
            }
            if (isMathExpression(child)) {
                expression = child
            }
        }

        if (castType == null) {
            throw ParsingException("No cast type found")
        }

        if (expression == null) {
            throw ParsingException("No expression found in cast")
        }

        val node = FunctionNode(InternalAccessor(castType), castType)
        node.addChild(parse(expression))
        return node
    }

    private fun postfix(root: PsiElement): Node {
        var expression: PsiElement? = null
        var operation: String? = null
        for (child in root.children) {
            val childType = child.node.elementType
            if (isMathExpression(child)) {
                expression = child
            } else if (childType === JavaTokenType.PLUSPLUS) {
                operation = "++"
            } else if (childType === JavaTokenType.MINUSMINUS) {
                operation = "--"
            }
        }

        if (operation == null) {
            throw ParsingException("Prefixed expression has no type")
        }
        if (expression == null) {
            throw ParsingException("No content found in prefixed expression")
        }

        val node = FunctionNode(InternalAccessor("()$operation"), "()$operation")
        node.addChild(parse(expression))
        return node
    }

    private fun prefix(root: PsiElement): Node {
        var expression: PsiElement? = null
        var operation: String? = null
        for (child in root.children) {
            val childType = child.node.elementType
            if (isMathExpression(child)) {
                expression = child
            } else if (childType === JavaTokenType.PLUS) {
                operation = "+"
            } else if (childType === JavaTokenType.MINUS) {
                operation = "-"
            } else if (childType === JavaTokenType.PLUSPLUS) {
                operation = "++"
            } else if (childType === JavaTokenType.MINUSMINUS) {
                operation = "--"
            } else if (childType === JavaTokenType.TILDE) {
                operation = "~"
            } else if (childType === JavaTokenType.EXCL) {
                operation = "!"
            }
        }

        if (operation == null) {
            throw ParsingException("Prefixed expression has no type")
        }
        if (expression == null) {
            throw ParsingException("No content found in prefixed expression")
        }

        val node = FunctionNode(InternalAccessor("$operation()"), "$operation()")
        node.addChild(parse(expression))
        return node
    }

    private fun parenthesis(root: PsiElement): Node {
        val node = FunctionNode(InternalAccessor("()"), "()")
        for (child in root.children) {
            if (child.node.elementType !== JavaTokenType.LPARENTH && child.node.elementType !== JavaTokenType.RPARENTH) {
                node.addChild(parse(child))
            }
        }
        return node
    }

    @Throws(ParsingException::class)
    private fun final(root: PsiElement): Node {
        return if (root is PsiReferenceExpression) {
            val resolved = root.resolve()
            FinalNode(root.text, getFullyQualifiedName(root, resolved, root.qualifiedName))
        } else {
            FinalNode(root.text, root.text)
        }
    }

    @Throws(ParsingException::class)
    private fun operation(root: PsiElement): Node {
        var operation: String? = null
        for (child in root.children) {
            operation = getOperator(child)
            if (operation != null) {
                break
            }
        }

        if (operation == null) {
            throw ParsingException("Unknown operation")
        }

        val node = OperationNode(operation)
        node.addChild(parse(root.firstChild))
        node.addChild(parse(root.lastChild))
        return node
    }

    private fun ifStatement(root: PsiElement): Node {
        val branches: MutableList<Node> = mutableListOf()

        var branchCondition: Node? = null

        for (child in flattenIfs(root)) {
            if (isMathExpression(child)) {
                branchCondition = parse(child)
            } else if (child.node.elementType === JavaElementType.BLOCK_STATEMENT) {
                val branchContent: MutableList<Statement> = mutableListOf()
                for (childChild in child.children) {
                    for (ifInnerChild in extractBlock(childChild)) {
                        if (isMathStatement(ifInnerChild)) {
                            val statement = statement(ifInnerChild)
                            if (statement != null) {
                                branchContent.add(statement)
                            }
                        } else if (isConditionalStatement(ifInnerChild)) {
                            branchContent.add(Statement(null, null, ifInnerChild))
                        }
                    }
                }
                val node = IfBranchNode(branchCondition, branchContent.toTypedArray())
                branches.add(node)
                branchCondition = null
            }
        }

        val node = IfStatementNode()
        branches.forEach(node::addChild)
        return node
    }

    private fun flattenIfs(root: PsiElement): MutableList<PsiElement> {
        val flattenedChildren: MutableList<PsiElement> = mutableListOf()
        for (child in root.children) {
            if (child.node.elementType === JavaElementType.IF_STATEMENT) {
                flattenedChildren.addAll(flattenIfs(child))
            } else {
                flattenedChildren.add(child)
            }
        }
        return flattenedChildren
    }

    @Throws(ParsingException::class)
    private fun polyadic(root: PsiElement): Node {
        var operation: String? = null
        val expressionContent: MutableList<PsiElement> = mutableListOf()
        for (child in root.children) {
            val childOperation = getOperator(child)
            if (childOperation != null) {
                operation = childOperation
            } else if (isMathExpression(child)) {
                expressionContent.add(child)
            }
        }
        if (operation == null) {
            throw ParsingException("Polyadic expression has no type")
        }
        if (expressionContent.isEmpty()) {
            throw ParsingException("No content found in function")
        }

        val node = OperationNode(operation)
        for (child in expressionContent) {
            node.addChild(parse(child))
        }
        return node
    }

    @Throws(ParsingException::class)
    private fun function(root: PsiElement): Node {
        var function: String? = null
        var accessor: MethodAccessor? = null
        var caller: Node? = null
        val functionContent: MutableList<Node> = mutableListOf()
        for (child in root.children) {
            if (child.node.elementType === JavaElementType.REFERENCE_EXPRESSION) {
                val childReferenceExpression = child as PsiReferenceExpression
                val resolved = childReferenceExpression.resolve()
                if (resolved is PsiMethod) {
                    if (isStatic(resolved)) {
                        function = childReferenceExpression.text
                        accessor = findSuperclassesWithMethod(resolved, resolved.name)
                    } else {
                        for (childChild in childReferenceExpression.children) {
                            if (isMathExpression(childChild)) {
                                caller = parse(childChild)
                            } else if (childChild.node.elementType === JavaTokenType.IDENTIFIER) {
                                accessor = findSuperclassesWithMethod(resolved, childChild.text)
                                function = childChild.text
                            }
                        }
                    }
                }
            } else if (child.node.elementType === JavaElementType.EXPRESSION_LIST) {
                for (parameter in child.children) {
                    if (parameter.node.elementType === JavaElementType.NEW_EXPRESSION) {
                        val node = stackArrayCreation(parameter)
                        if (node != null) {
                            functionContent.add(node)
                            continue
                        }
                    }
                    if (isMathExpression(parameter)) {
                        functionContent.add(parse(parameter))
                    }
                }
            }
        }

        if (function == null) {
            throw ParsingException("Function has no type")
        }

        if (accessor == null) {
            throw ParsingException("No accessor created")
        }

        val node = if (caller == null) {
            FunctionNode(accessor, function)
        } else {
            DynamicFunctionNode(accessor, function, caller)
        }

        for (child in functionContent) {
            node.addChild(child)
        }
        return node
    }

    private fun stackArrayCreation(root: PsiElement): ArrayCreationNode? {
        if (root.node.elementType === JavaElementType.ARRAY_INITIALIZER_EXPRESSION) {
            val node = ArrayCreationNode()
            for (childInitializer in root.children) {
                if (childInitializer.node.elementType === JavaElementType.NEW_EXPRESSION) {
                    val nodeIn = stackArrayCreation(childInitializer)
                    if (nodeIn != null) {
                        node.addChild(nodeIn)
                        continue
                    }
                }
                if (childInitializer.node.elementType === JavaElementType.ARRAY_INITIALIZER_EXPRESSION) {
                    val nodeIn = stackArrayCreation(childInitializer)
                    if (nodeIn != null) {
                        node.addChild(nodeIn)
                        continue
                    }
                }
                if (isMathExpression(childInitializer)) {
                    node.addChild(parse(childInitializer))
                }
            }
            return node
        }

        for (childParameter in root.children) {
            if (childParameter.node.elementType === JavaElementType.ARRAY_INITIALIZER_EXPRESSION) {
                val node = ArrayCreationNode()
                for (childInitializer in childParameter.children) {
                    if (childInitializer.node.elementType === JavaElementType.NEW_EXPRESSION) {
                        val nodeIn = stackArrayCreation(childInitializer)
                        if (nodeIn != null) {
                            node.addChild(nodeIn)
                            continue
                        }
                    }
                    if (childInitializer.node.elementType === JavaElementType.ARRAY_INITIALIZER_EXPRESSION) {
                        val nodeIn = stackArrayCreation(childInitializer)
                        if (nodeIn != null) {
                            node.addChild(nodeIn)
                            continue
                        }
                    }
                    if (isMathExpression(childInitializer)) {
                        node.addChild(parse(childInitializer))
                    }
                }
                return node
            }
        }
        return null
    }

    private fun getOperator(child: PsiElement): String? {
        val childType = child.node.elementType
        return when {
            childType === JavaTokenType.PLUS -> {
                "+"
            }

            childType === JavaTokenType.MINUS -> {
                "-"
            }

            childType === JavaTokenType.ASTERISK -> {
                "*"
            }

            childType === JavaTokenType.DIV -> {
                "/"
            }

            childType === JavaTokenType.PERC -> {
                "%"
            }

            childType === JavaTokenType.AND -> {
                "&"
            }

            childType === JavaTokenType.OR -> {
                "|"
            }

            childType === JavaTokenType.XOR -> {
                "^"
            }

            childType === JavaTokenType.LTLT -> {
                "<<"
            }

            childType === JavaTokenType.GTGT -> {
                ">>"
            }

            childType === JavaTokenType.GTGTGT -> {
                ">>>"
            }

            childType === JavaTokenType.OROR -> {
                "||"
            }

            childType === JavaTokenType.ANDAND -> {
                "&&"
            }

            childType === JavaTokenType.EQEQ -> {
                "=="
            }

            childType === JavaTokenType.NE -> {
                "!="
            }

            childType === JavaTokenType.LE -> {
                "<="
            }

            childType === JavaTokenType.GE -> {
                ">="
            }

            childType === JavaTokenType.LT -> {
                "<"
            }

            childType === JavaTokenType.GT -> {
                ">"
            }

            else -> {
                null
            }
        }
    }

    private fun getAssignOperator(element: PsiElement): String? {
        val elementType = element.node.elementType
        return when {
            elementType === JavaTokenType.EQ -> {
                "="
            }

            elementType === JavaTokenType.PLUSEQ -> {
                "+="
            }

            elementType === JavaTokenType.MINUSEQ -> {
                "-="
            }

            elementType === JavaTokenType.ASTERISKEQ -> {
                "*="
            }

            elementType === JavaTokenType.DIVEQ -> {
                "/="
            }

            elementType === JavaTokenType.PERCEQ -> {
                "%="
            }

            elementType === JavaTokenType.ANDEQ -> {
                "&="
            }

            elementType === JavaTokenType.OREQ -> {
                "|="
            }

            elementType === JavaTokenType.XOREQ -> {
                "^="
            }

            elementType === JavaTokenType.LTLTEQ -> {
                "<<="
            }

            elementType === JavaTokenType.GTGTEQ -> {
                ">>="
            }

            elementType === JavaTokenType.GTGTGTEQ -> {
                ">>>="
            }

            else -> {
                null
            }
        }
    }

    private fun getPrimitiveType(child: PsiElement): String? {
        return when (child.node.elementType) {
            JavaTokenType.DOUBLE_KEYWORD -> {
                "double"
            }

            JavaTokenType.FLOAT_KEYWORD -> {
                "float"
            }

            JavaTokenType.LONG_KEYWORD -> {
                "long"
            }

            JavaTokenType.INT_KEYWORD -> {
                "int"
            }

            JavaTokenType.CHAR_KEYWORD -> {
                "char"
            }

            JavaTokenType.SHORT_KEYWORD -> {
                "short"
            }

            JavaTokenType.BYTE_KEYWORD -> {
                "byte"
            }

            else -> {
                null
            }
        }
    }

    private fun isMathExpression(element: PsiElement): Boolean {
        val elementType = element.node.elementType
        return (elementType === JavaElementType.BINARY_EXPRESSION
                || elementType === JavaElementType.METHOD_CALL_EXPRESSION
                || elementType === JavaElementType.REFERENCE_EXPRESSION
                || elementType === JavaElementType.LITERAL_EXPRESSION
                || elementType === JavaElementType.PARENTH_EXPRESSION
                || elementType === JavaElementType.POLYADIC_EXPRESSION
                || elementType === JavaElementType.PREFIX_EXPRESSION
                || elementType === JavaElementType.POSTFIX_EXPRESSION
                || elementType === JavaElementType.TYPE_CAST_EXPRESSION
                || elementType === JavaElementType.CONDITIONAL_EXPRESSION
                || elementType === JavaElementType.NEW_EXPRESSION
                || elementType === JavaElementType.ARRAY_ACCESS_EXPRESSION)
    }

    private fun isMathStatement(element: PsiElement): Boolean {
        val elementType = element.node.elementType
        if (elementType === JavaElementType.DECLARATION_STATEMENT) {
            return true
        } else if (elementType === JavaElementType.EXPRESSION_STATEMENT) {
            if (element.children.isNotEmpty() && element.children[0].node.elementType === JavaElementType.ASSIGNMENT_EXPRESSION) {
                return true
            }
        }
        return false
    }

    private fun isConditionalStatement(element: PsiElement): Boolean {
        val elementType = element.node.elementType
        if (elementType === JavaElementType.IF_STATEMENT) {
            if (element.children.isNotEmpty()) {
                for (child in element.children) {
                    if (child.node.elementType === JavaElementType.BLOCK_STATEMENT && child.children.isNotEmpty()) {
                        for (ifChild in extractBlock(child.children[0])) {
                            if (isMathExpression(ifChild) || isMathStatement(ifChild)) {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    private fun extractBlock(element: PsiElement): Array<PsiElement> {
        val elementType = element.node.elementType
        if (elementType === JavaElementType.CODE_BLOCK) {
            val blockContent: MutableList<PsiElement> = mutableListOf()
            for (child in element.children) {
                blockContent.addAll(extractBlock(child))
            }
            return blockContent.toTypedArray()
        }
        return arrayOf(element)
    }

    private fun getFullyQualifiedName(
        element: PsiQualifiedReference,
        resolved: PsiElement?,
        fallback: String,
    ): String {
        return when (resolved) {
            is PsiClass -> {
                return resolved.qualifiedName ?: fallback
            }

            is PsiMember -> {
                resolved.containingClass?.qualifiedName + "." + element.referenceName
            }

            else -> {
                fallback
            }
        }
    }

    private fun isStatic(resolved: PsiElement?): Boolean {
        if (resolved is PsiMember) {
            val containingClass: PsiClass? = resolved.containingClass
            if (containingClass != null) {
                return resolved.hasModifierProperty(PsiModifier.STATIC)
            }
        }
        throw ParsingException("Element is not a class member")
    }

    private fun findConstructor(resolved: PsiMethod): ConstructorAccessor? {
        val containingClass = resolved.containingClass
        if (containingClass != null) {
            val fqn = resolved.containingClass?.qualifiedName ?: return null

            val parameters = resolved.parameterList.parameters.mapNotNull {
                when (val type = it.typeElement?.type) {
                    is PsiClassType -> {
                        type.resolve()?.qualifiedName
                    }

                    is PsiPrimitiveType -> {
                        type.name
                    }

                    is PsiArrayType -> {
                        type.getCanonicalText(false)
                    }

                    else -> {
                        null
                    }
                }
            }

            return ConstructorAccessor(fqn, parameters)
        }
        return null
    }

    private fun findSuperclassesWithMethod(resolved: PsiMethod, methodName: String?): MethodAccessor? {
        if (methodName != null) {
            val containingClass = resolved.containingClass
            if (containingClass != null) {
                val parameters = resolved.parameterList.parameters.mapNotNull {
                    when (val type = it.typeElement?.type) {
                        is PsiClassType -> {
                            type.resolve()?.qualifiedName
                        }

                        is PsiPrimitiveType -> {
                            type.name
                        }

                        is PsiArrayType -> {
                            type.getCanonicalText(false)
                        }

                        else -> {
                            null
                        }
                    }
                }

                val classes =
                    searchSuperclassesWithMethod(containingClass, methodName, parameters).distinct()
                return if (classes.isNotEmpty()) {
                    MethodAccessor(classes.map { it.qualifiedName + "." + methodName }, parameters)
                } else {
                    null
                }
            }
        }
        return null
    }

    private fun searchSuperclassesWithMethod(
        psiClass: PsiClass,
        methodName: String,
        parameters: List<String>,
    ): MutableList<PsiClass> {
        val result: MutableList<PsiClass> = mutableListOf()
        val methods = psiClass.findMethodsByName(methodName, false)
        if (methods.isNotEmpty() && methods.any {
                it.parameterList.parameters.mapNotNull { itIt ->
                    when (val type = itIt.type) {
                        is PsiClassType -> {
                            type.resolve()?.qualifiedName
                        }

                        is PsiPrimitiveType -> {
                            type.name
                        }

                        is PsiArrayType -> {
                            type.getCanonicalText(false)
                        }

                        else -> {
                            null
                        }
                    }
                } == parameters
            }) {
            result.add(psiClass)
        }

        val superClass = psiClass.superClass
        if (superClass != null) {
            result.addAll(searchSuperclassesWithMethod(superClass, methodName, parameters))
        }

        val superInterfaces: MutableList<PsiClass> = psiClass.interfaces.toCollection(mutableListOf())
        for (superInterface in superInterfaces) {
            result.addAll(searchSuperclassesWithMethod(superInterface, methodName, parameters))
        }

        return result
    }
}