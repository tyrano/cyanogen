package eu.tyrano.cyanogen

class ParsingException(message: String?): Exception(message)