package eu.tyrano.cyanogen

import com.intellij.psi.PsiElement

data class Statement(val name: String?, val operator: String?, val expression: PsiElement)