# Cyanogen IntelliJ Plugin

Cyanogen is an IntelliJ IDEA plugin designed to enhance your development experience
by rendering Java mathematical expressions and statements in LaTeX.
This visualization makes it easier to understand and work with complex formulas directly in your code.

## Configurability

- **Customizable Rendering**: Tailor how fields, methods, and object creations are displayed.
- **Settings Management**: Import/export settings for consistent workflows across projects.

## Getting Started

Explore the full documentation in the wiki to unlock all the plugin's features.

[Download me here!](https://plugins.jetbrains.com/plugin/22736-cyanogen/)
